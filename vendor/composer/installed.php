<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.6',
      'version' => '2.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd9d313a36c872fd6ee06d9a6cbcf713eaa40f024',
    ),
    'illuminate/collections' => 
    array (
      'pretty_version' => 'v9.48.0',
      'version' => '9.48.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '56c63a83d985a3bc6db31f4bd8b3b297dc40284e',
    ),
    'illuminate/conditionable' => 
    array (
      'pretty_version' => 'v9.48.0',
      'version' => '9.48.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b40f51ccb07e0e7b1ec5559d8db9e0e2dc51883',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => 'v9.48.0',
      'version' => '9.48.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4b79cdbda0a6e8fe520fccc0e8c4ba1fed2a2d07',
    ),
    'illuminate/macroable' => 
    array (
      'pretty_version' => 'v9.48.0',
      'version' => '9.48.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e3bfaf6401742a9c6abca61b9b10e998e5b6449a',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => 'v9.48.0',
      'version' => '9.48.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8c77cfd609addaba90a5efbf585c091c9f9e6c79',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.65.0',
      'version' => '2.65.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '09acf64155c16dc6f580f36569ae89344e9734a3',
    ),
    'nordsoftware/lumen-cors' => 
    array (
      'pretty_version' => '3.5.0',
      'version' => '3.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a8822a87d1c1aff7c11141c617bd8f37a9c033d9',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c71ecc56dfe541dbd90c5360474fbc405f8d5963',
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '764e0b3939f5ca87cb904f570ef9be2d78a07865',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8ad114f6b39e2c98a8b0e3bd907732c207c2b534',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.27.0',
      'version' => '1.27.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a6ff3f1959bb01aefccb463a0f2cd3d3d2fd936',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v6.2.3',
      'version' => '6.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a2a15404ef4c15d92c205718eb828b225a144379',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v3.2.0',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '68cce71402305a015f8c1589bfada1280dc64fe7',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3|3.0',
      ),
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b56450eed252f6801410d810c8e1727224ae0743',
    ),
  ),
);
