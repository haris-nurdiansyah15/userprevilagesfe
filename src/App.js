import "antd/dist/antd.css";
import "./App.css";
import Main from "./Layouts/Main";
import { UserProvider } from "./Store/UserContext";

function App() {
  return (
    <UserProvider>
      <Main />
    </UserProvider>
  );
}

export default App;
