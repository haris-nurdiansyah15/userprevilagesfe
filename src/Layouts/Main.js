import React, { useState, useContext, useEffect } from "react";
import Navbar from "./Navbar";
import Sidebar from "./Sidebar";
import CustomFooter from "./Footer";
import Section from "./Section";
import { UserContext } from "../Store/UserContext";

import { Layout } from "antd";

import {
  BrowserRouter as Router,
  useLocation,
  useHistory,
} from "react-router-dom";
const { Content } = Layout;

const Main = () => {
  const [user] = useContext(UserContext);
  return (
    <Router>
      <Layout style={{ minHeight: "100vh" }}>
        {user && <Sidebar />}
        <Layout className="site-layout">
          <Navbar />
          <Content className="main-content" style={{width: user ? '95%' : '80%'}}>
            <Section />
          </Content>
          <CustomFooter />
        </Layout>
      </Layout>
    </Router>
  );
};

export default Main;

// {}
