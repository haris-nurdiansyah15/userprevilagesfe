import React, { useState, useContext } from "react";
import { UserContext } from "../Store/UserContext";
import { Link } from "react-router-dom";
import {
  UpCircleFilled,
  PoweroffOutlined,
} from "@ant-design/icons";

const Navbar = () => {
  const [user, setUser] = useContext(UserContext);

  const handeLogout = (e) => {
    e.preventDefault();
    setUser(null);
    localStorage.removeItem("user");
  };

  return (
    <>
      <header className="header">
        <ul>
          {!user ? (
            <li>
              <Link to="/login">
                Login <UpCircleFilled />
              </Link>
            </li>
          ): (
            <li>
              <a href="#" onClick={handeLogout}>
                Logout &nbsp; <PoweroffOutlined />
              </a>
            </li>
          )}
        </ul>
      </header>
    </>
  );
};

export default Navbar;

// {}
