import React from "react";

import { Layout, BackTop } from "antd";
import { ArrowUpOutlined } from "@ant-design/icons";
const { Footer } = Layout;

const style = {
  height: 50,
  width: 50,
  lineHeight: "50px",
  borderRadius: '100%',
  backgroundColor: "#001529",
  color: "#fff",
  textAlign: "center",
  fontSize: 20,
};

class CustomFooter extends React.Component {
  render() {
    return (
      <>
        <BackTop duration="600">
          <div style={style}>
            <ArrowUpOutlined />
          </div>
        </BackTop>
        <Footer
          style={{
            textAlign: "center",
            color: "#001529",
            backgroundColor: "white",
            paddingBottom: 40,
            height: 48,
          }}
        >
          Created by Haris Nurdiansyah
        </Footer>
      </>
    );
  }
}

export default CustomFooter;

// {}
