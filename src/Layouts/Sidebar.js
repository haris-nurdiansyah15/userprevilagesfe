import React, { useState, useContext } from "react";
import { Link } from "react-router-dom";
import { UserContext } from "../Store/UserContext";
import { Layout, Menu, Avatar } from "antd";
import {
  SettingFilled,
} from "@ant-design/icons";

const { Sider } = Layout;
const { SubMenu } = Menu;

const Sidebar = () => {
  const [user] = useContext(UserContext);
  const [access] = useState(user.access_menu);
  const [collapsed, setCollapsed] = useState(false);

  console.log(access);

  const onCollapse = (collapsed) => {
    setCollapsed(collapsed);
  };

  return (
    <Sider collapsible collapsed={collapsed} onCollapse={onCollapse} width={200}>
      <Menu theme="dark"  mode="inline">
        <Menu.Item style={{justifyContent: "center", textAlign: "center", display: "flex", marginTop: 30}}>
          <Avatar
          size={35}
            style={{
              color: "#f56a00",
              backgroundColor: "#fde3cf",
              margin: "0 auto",
            }}
          >
            {user.username[0].toUpperCase()}
          </Avatar>
        </Menu.Item>
        
        {
          !collapsed ? (
            <Menu.Item className="sider-menu-item" style={{textAlign: "center"}}>
              Hi, {user.username}
            </Menu.Item>
          ) : ''
        }

        {
          access.map((item) => (

            <>
              <SubMenu
                className="sider-menu-item"
                icon={<SettingFilled />}
                title={item.name}
              >
                {
                  item.sub_menu.map((item) => (

                    <>
                      <Menu.Item className="sider-menu-item">
                        <Link to={item.uri}>
                          &nbsp;&nbsp;&nbsp;{item.name}
                        </Link>
                      </Menu.Item>
                    </>

                  ))
                }
              </SubMenu>
           </>

          ))
        }
      </Menu>
    </Sider>
  );
};

export default Sidebar;

// {}
