import React, { useContext } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import FormLogin from "../Components/auth/Login";
import { UserContext } from "../Store/UserContext";
import FormMenu from "../Components/privateComponent/FormMenu";
import MenuTable from "../Components/privateComponent/MenuTable";
import UserTable from "../Components/privateComponent/UserTable";
import FormUser from "../Components/privateComponent/FormUser";

const RouteApp = () => {
  const [user] = useContext(UserContext);

  const PrivateRoute = ({ ...rest }) => {
    if (user) {
      return <Route {...rest} />;
    } else {
      return <Redirect to="/login" />;
    }
  };

  const LoginRoute = ({ ...rest }) => {
    if (user) {
      return <Redirect to="/table-user" />;
    } else {
      return <Route {...rest} />;
    }
  };

  return (
    <div>
      <Switch>
        <PrivateRoute exact path="/table-user">
          <UserTable />
        </PrivateRoute>
        <PrivateRoute exact path="/user/create">
          <FormUser />
        </PrivateRoute>
        <PrivateRoute exact path="/user/:id/edit">
          <FormUser />
        </PrivateRoute>
        <PrivateRoute exact path="/table-menu">
          <MenuTable />
        </PrivateRoute>
        <PrivateRoute exact path="/create-menu">
          <FormMenu />
        </PrivateRoute>
        <PrivateRoute exact path="/menu/:id/edit">
          <FormMenu />
        </PrivateRoute>
        <LoginRoute exact path="/login">
          <FormLogin />
        </LoginRoute>
        <LoginRoute exact path="/">
          <FormLogin />
        </LoginRoute>
      </Switch>
    </div>
  );
};

export default RouteApp;
