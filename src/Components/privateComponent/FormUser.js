import React, { useEffect, useState } from "react";
import axios from "axios";
import { useContext } from "react";
import { useParams, useHistory } from "react-router-dom";
import { UserContext } from "../../Store/UserContext";
import { VerticalAlignBottomOutlined, EyeTwoTone, EyeInvisibleOutlined } from  "@ant-design/icons";
import { Input, Button, Divider, Row, Col, Checkbox } from "antd";

const FormUser = () => {

    let { id } = useParams();
    let history = useHistory();
    const [fetchTrigger, setFetchTrigger] = useState(true);
    const [menus, setMenus] = useState([]);
    const [input, setInput] = useState({
        username: "",
        password: "",
        accesses: []
    });

    const [user] = useContext(UserContext);
    let token = user ? user.token : null;

    useEffect(() => {

        const fetchData = async () => {
        if (id !== undefined) {
            const result = await axios.get(
            `http://localhost:8000/api/user/detail/${id}`,
            { headers: { Authorization: "Bearer " + token } }
            );
            const { username, accesses_id } = result.data.data;
            setInput({ ...input, username, accesses: [...accesses_id] });
        }
        };

        const getMenu = async () => {
            const result = await axios.get(
            `http://localhost:8000/api/menu/parent`,
            { headers: { Authorization: "Bearer " + token } }
            );
            const data = result.data.data.map((menu) => {
            return menu
            });
            setMenus([...data]);
        };

        if (fetchTrigger) {
        fetchData();
        getMenu();
        setFetchTrigger(false);
        }

    }, [fetchTrigger]);

    const handleSubmit = (e) => {
        e.preventDefault();
        const { username, password, accesses } = input;
        if (id === undefined) {
        axios
            .post(
            `http://localhost:8000/api/user/store`,
            { username, password, access_menus: accesses },
            { headers: { Authorization: "Bearer " + token } }
            )
            .then(() => history.push('/table-user'))
            .catch((err) => {
                const error = err.response.data.message; 
                alert(error);
            });
        } else {
        axios
            .put(
            `http://localhost:8000/api/user/store/${id}`,
            { username, password, access_menus: accesses },
            { headers: { Authorization: "Bearer " + token } }
            )
            .then(() => {
                history.push(`/table-user`);
            })
            .catch((err) => {
                const error = err.response.data.message; 
                alert(error);
            });
        }

        setInput({
            username: "",
            password: ""
        });
    };

    const handleChange = (e) => {
        const { name, value } = e.target;
        setInput({
        ...input,
        [name]: value,
        });
    };

    const handleCheckParent = (e) => {
        const checked = e.target.checked;
        console.log(e.target.options);
    }

    const handleCheck = (e) => {
        const checked = e.target.checked;
        if (checked) {
            setInput({...input, accesses: [...input.accesses, e.target.value]});
        } else {
            const result = input.accesses.filter(access => access !== e.target.value);
            setInput({...input, accesses: [...result]});
        }
    };

    const generatePassword = () => {
        var chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()';
        var charLength = chars.length;
        var result = '';
        for ( var i = 0; i < 50; i++ ) {
            result += chars.charAt(Math.floor(Math.random() * charLength));
        }
        setInput({
            ...input,
            password: result
        });
    }

    return (
        <div
        className="site-layout-background content"
        style={{
            padding: 20,
            paddingRight: 50,
            paddingLeft: 50,
            paddingBottom: 50,
        }}
        >
        <Divider orientation="center">
            <h2>Form { id ? 'Edit' : 'Create' } User</h2>
        </Divider>
        <form onSubmit={handleSubmit}>
            <Row>
            <Col flex="100px">
                <label for="username" style={{ float: "right", marginRight: 20 }}>
                Username:
                </label>
            </Col>
            <Col flex="auto">
                <Input
                id="username"
                name="username"
                onChange={handleChange}
                value={input.username}
                placeholder="Input username..."
                required
                />
            </Col>
            </Row>
            <br />
            <Row>
            <Col flex="100px">
                <label for="password" style={{ float: "right", marginRight: 20 }}>
                Password:
                </label>
            </Col>
            <Col flex="auto">
                <Row>
                    <Col flex="auto" style={{ marginRight: 10 }}>
                        <Input.Password
                            id="password"
                            name="password"
                            value={input.password}
                            onChange={handleChange}
                            placeholder="input password"
                            iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                        />
                    </Col>
                    <Col flex="150px">
                        <Button type="primary" onClick={() => generatePassword()}>
                            Generate Password
                        </Button>
                    </Col>
                </Row>
            </Col>
            </Row>
            <br />
            <Row>
            <Col flex="100px">
                <label for="username" style={{ float: "right", marginRight: 20 }}>
                Access Menu:
                </label>
            </Col>
            <Col flex="auto">
                {
                    menus.map((menu) => (
                        <>
                            <Checkbox onClick={handleCheckParent} defaultChecked={input.accesses !== undefined ? input.accesses.includes(menu.id) : false} onChange={handleCheck} value={menu.id}>{menu.name}</Checkbox>
                            <div style={{ marginTop: 10, marginBottom: 10 }}>
                                {
                                    menu.sub_menu.map((submenu) => (
                                        <Checkbox defaultChecked={input.accesses !== undefined ? input.accesses.includes(submenu.id) : false} onChange={handleCheck} value={submenu.id}>{submenu.name}</Checkbox>
                                    ))
                                }
                            </div>
                        </>
                    ))
                }
            </Col>
            </Row>
            <br />
            <Row>
            <Col flex="100px"></Col>
            <Col flex="auto">
                <Button type="primary" htmlType="submit">
                Save <VerticalAlignBottomOutlined />
                </Button>
            </Col>
            </Row>
        </form>
        </div>
    );
};

export default FormUser;

// {} []
