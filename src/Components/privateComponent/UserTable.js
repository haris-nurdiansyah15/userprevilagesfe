import React, { useContext, useEffect, useState } from "react";
import { UserContext } from "../../Store/UserContext";
import axios from "axios";
import { useHistory } from "react-router-dom";

import {
  FormOutlined,
  DeleteOutlined,
  ExclamationCircleOutlined,
  PlusOutlined
} from "@ant-design/icons";

import { Input, Modal, Table, Button, Tooltip, Space, Divider } from "antd";
const { Search } = Input;
const { confirm } = Modal;

const UserTable = () => {

  const [user, setUser] = useContext(UserContext);
  const [users, setUsers] = useState([]);
  const [inputSearch, setInputSearch] = useState("");
  const [fetchTrigger, setFetchTrigger] = useState(true);

  let history = useHistory();
  let token = user ? user.token : null;

  const columns = [
    {
      title: "Username",
      dataIndex: "username",
      defaultSortOrder: "descend",
    },
    {
      title: "Action",
      dataIndex: "id",
      key: "id",
      render: (id) => {
        return (
          <>
            <Space direction="horizontal">
              <Tooltip title="Edit">
                <Button
                  type="primary"
                  icon={<FormOutlined />}
                  onClick={() => {
                    goToformEdit(id);
                  }}
                />
              </Tooltip>
              <Tooltip title="Hapus">
                <Button
                  type="danger"
                  icon={<DeleteOutlined />}
                  onClick={() => {
                    deleteUser(id);
                  }}
                />
              </Tooltip>
            </Space>
          </>
        );
      },
    },
  ];

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(
        `http://localhost:8000/api/user`,
        { headers: { Authorization: "Bearer " + token } }
      );
      setUsers(
        result.data.data.map((user) => {
          return {
            id: user.id,
            username: user.username
          };
        })
      );
    };
    if (fetchTrigger) {
      fetchData();
      setFetchTrigger(false);
    }
  }, [fetchTrigger]);

  const goToformEdit = (id) => {
    history.push(`/user/${id}/edit`);
  };

  const goToformAddUser = () => {
    history.push(`/user/create`);
  };

  const deleteUser = (id) => {
    let IDUser = parseInt(id);
    confirm({
      title: "Do you want to delete these user ?",
      icon: <ExclamationCircleOutlined />,
      onOk() {
        axios
          .delete(
            `http://localhost:8000/api/user/delete/${IDUser}`,
            { headers: { Authorization: "Bearer " + token } }
          )
          .then((response) => {
            const message = response.data.message;
            alert(message);
            setFetchTrigger(true);
          })
          .catch((err) => {
          const error = err.response.data.message;
            alert(error);
          });
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };

  const handleSearch = (value) => {
    if (value) {
      let result = users.filter((item) => {
        return item.username === value;
      });
      console.log(result);
      setUsers([...result]);
      setInputSearch("");
    } else {
      setFetchTrigger(true);
    }
    // {} []
  };

  function onChange(pagination, filters, sorter, extra) {
    console.log("params", pagination, filters, sorter, extra);
  }

  return (
    <div className="site-layout-background content" style={{ padding: 30 }}>
      <div
        style={{
          paddingBottom: 0,
          paddingTop: 0.4,
          backgroundColor: "#e6f7ff"
        }}
      >
      </div>
      <div style={{ marginBottom: "10px", display: "flex", justifyContent: "end" }}>
        <Search
            placeholder="Search by name..."
            value={inputSearch}
            onChange={(e) => {
              setInputSearch(e.target.value);
            }}
            onSearch={handleSearch}
            enterButton
            autoFocus
            style={{ marginBottom: 15, marginRight: "10px",width: "100%" }}
          />
        <Button type="primary" onClick={() => goToformAddUser()}>Add User <PlusOutlined /></Button>
      </div>
      <Table
        columns={columns}
        dataSource={users}
        onChange={onChange}
        style={{ width: "100%" }}
      />
    </div>
  );
};

export default UserTable;

// {} []
