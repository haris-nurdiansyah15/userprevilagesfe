import React, { useContext, useEffect, useState } from "react";
import { UserContext } from "../../Store/UserContext";
import axios from "axios";
import { useHistory } from "react-router-dom";

import {
  FormOutlined,
  DeleteOutlined,
  ExclamationCircleOutlined,
  PlusOutlined
} from "@ant-design/icons";

import { Input, Modal, Table, Button, Tooltip, Space, Divider } from "antd";
const { Search } = Input;
const { confirm } = Modal;

const MenuTable = () => {

  const [user, setUser] = useContext(UserContext);
  const [menus, setMenus] = useState([]);
  const [inputSearch, setInputSearch] = useState("");
  const [fetchTrigger, setFetchTrigger] = useState(true);

  let history = useHistory();
  let token = user ? user.token : null;

  const columns = [
    {
      title: "Menu Name",
      dataIndex: "name",
      defaultSortOrder: "descend",
    },
    {
      title: "Uri",
      dataIndex: "uri",
      defaultSortOrder: "descend",
    },
    {
      title: "Parent",
      dataIndex: "parent",
      defaultSortOrder: "descend",
    },
    {
      title: "Action",
      dataIndex: "id",
      key: "id",
      render: (id) => {
        return (
          <>
            <Space direction="horizontal">
              <Tooltip title="Edit">
                <Button
                  type="primary"
                  icon={<FormOutlined />}
                  onClick={() => {
                    goToformEdit(id);
                  }}
                />
              </Tooltip>
              <Tooltip title="Hapus">
                <Button
                  type="danger"
                  icon={<DeleteOutlined />}
                  onClick={() => {
                    deleteMenu(id);
                  }}
                />
              </Tooltip>
            </Space>
          </>
        );
      },
    },
  ];

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(
        `http://localhost:8000/api/menu`,
        { headers: { Authorization: "Bearer " + token } }
      );
      setMenus(
        result.data.data.map((menu) => {
          return {
            id: menu.id,
            name: menu.name,
            uri: menu.uri,
            parent: menu.parent_menu ? menu.parent_menu.name : 'Nothing',
          };
        })
      );
    };
    if (fetchTrigger) {
      fetchData();
      setFetchTrigger(false);
    }
  }, [fetchTrigger]);

  const goToformEdit = (id) => {
    history.push(`/menu/${id}/edit`);
  };

  const goToformAddMenu = () => {
    history.push(`/create-menu`);
  };

  const deleteMenu = (id) => {
    let IDMenu = parseInt(id);
    confirm({
      title: "Do you want to delete these menu ?",
      icon: <ExclamationCircleOutlined />,
      onOk() {
        axios
          .delete(
            `http://localhost:8000/api/menu/delete/${IDMenu}`,
            { headers: { Authorization: "Bearer " + token } }
          )
          .then((response) => {
            const message = response.data.message;
            alert(message);
            setFetchTrigger(true);
          })
          .catch((err) => {
            const error = err.response.data.message;
            alert(error);
          });
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };

  const handleSearch = (value) => {
    if (value) {
      let result = menus.filter((menu) => {
        return menu.name === value;
      });
      setMenus([...result]);
      setInputSearch("");
    } else {
      setFetchTrigger(true);
    }
    // {} []
  };

  function onChange(pagination, filters, sorter, extra) {
    console.log("params", pagination, filters, sorter, extra);
  }

  return (
    <div className="site-layout-background content" style={{ padding: 30 }}>
      <div
        style={{
          paddingBottom: 0,
          paddingTop: 0.4,
          backgroundColor: "#e6f7ff"
        }}
      >
      </div>
      <div style={{ marginBottom: "10px", display: "flex", justifyContent: "end" }}>
        <Search
            placeholder="Search by name..."
            value={inputSearch}
            onChange={(e) => {
              setInputSearch(e.target.value);
            }}
            onSearch={handleSearch}
            enterButton
            autoFocus
            style={{ marginBottom: 15, marginRight: "10px",width: "100%" }}
          />
        <Button type="primary" onClick={() => goToformAddMenu()}>Add Menu <PlusOutlined /></Button>
      </div>
      <Table
        columns={columns}
        dataSource={menus}
        onChange={onChange}
        style={{ width: "100%" }}
      />
    </div>
  );
};

export default MenuTable;

// {} []
