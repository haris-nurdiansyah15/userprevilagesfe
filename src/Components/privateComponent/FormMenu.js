import React, { useEffect, useState } from "react";
import axios from "axios";
import { useContext } from "react";
import { useParams, useHistory } from "react-router-dom";
import { UserContext } from "../../Store/UserContext";
import { VerticalAlignBottomOutlined } from  "@ant-design/icons";
import { Input, Button, Divider, Row, Col, Select } from "antd";

const FormMenu = () => {

  let { id } = useParams();
  let history = useHistory();
  const [fetchTrigger, setFetchTrigger] = useState(true);
  const [selectParent, setSelectParent] = useState([]);
  const [parentValue, setParentValue] = useState(null);
  const [input, setInput] = useState({
    name: "",
    uri: "",
    parent_id: null,
  });

  const [user] = useContext(UserContext);
  let token = user ? user.token : null;

  useEffect(() => {

    const fetchData = async () => {
      if (id !== undefined) {
        const result = await axios.get(
          `http://localhost:8000/api/menu/detail/${id}`,
          { headers: { Authorization: "Bearer " + token } }
        );
        const { name, uri, parent_id } = result.data.data;
        setInput({ name, uri, parent_id });
        setParentValue(parent_id);
      }
    };

    const getParent = async () => {
      const result = await axios.get(
        `http://localhost:8000/api/menu/parent`,
        { headers: { Authorization: "Bearer " + token } }
      );
      const data = result.data.data.map(({id, name}) => {
        return {id, name}
      });
      setSelectParent([...data]);
    };

    if (fetchTrigger) {
      fetchData();
      getParent();
      setFetchTrigger(false);
    }

  }, [fetchTrigger]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setInput({
      ...input,
      [name]: value,
    });
  };

  const handleSelectParentMenu = (e) => {
    setParentValue(e);
    setInput({
      ...input,
      parent_id: e
    });
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    const {
      name,
      uri,
      parent_id
    } = input;
    if (id === undefined) {
      axios
        .post(
          `http://localhost:8000/api/menu/store/`,
          {
            name,
            uri: uri ?? null,
            parent_id: parent_id ?? null
          },
          { headers: { Authorization: "Bearer " + token } }
        )
        .then((res) => history.push('/table-menu'))
        .catch((err) => {
          alert(err.message);
        });
    } else {
      axios
        .put(
          `http://localhost:8000/api/menu/store/${id}`,
          {
            name,
            uri: uri ?? null,
            parent_id: parent_id ?? null
          },
          { headers: { Authorization: "Bearer " + token } }
        )
        .then(() => history.push(`/table-menu`))
        .catch((err) => {
          alert(err.message);
        });
    }

    setInput({
      name: "",
      uri: "",
      parent_id: 0
    });
  };

  return (
    <div
      className="site-layout-background content"
      style={{
        padding: 20,
        paddingRight: 50,
        paddingLeft: 50,
        paddingBottom: 50,
      }}
    >
      <Divider orientation="center">
        <h2>Form Create Menu</h2>
      </Divider>
      <form onSubmit={handleSubmit}>
        <Row>
          <Col flex="100px">
            <label for="name" style={{ float: "right", marginRight: 20 }}>
              Menu Name:
            </label>
          </Col>
          <Col flex="auto">
            <Input
              id="name"
              name="name"
              onChange={handleChange}
              value={input.name}
              placeholder="Inpit menu name..."
              required
            />
          </Col>
        </Row>
        <br />
        <Row>
          <Col flex="100px">
            <label for="uri" style={{ float: "right", marginRight: 20 }}>
              Uri:
            </label>
          </Col>
          <Col flex="auto">
            <Input
              id="uri"
              name="uri"
              onChange={handleChange}
              value={input.uri}
              placeholder="Inpit uri menu..."
            />
          </Col>
        </Row>
        <br />
        <Row>
          <Col flex="100px">
            <label for="title" style={{ float: "right", marginRight: 20 }}>
              Parent:
            </label>
          </Col>
          <Col flex="auto">
            <Select
              value={parentValue}
              placeholder="Choose Parent"
              style={{ width: "100%" }}
              onChange={handleSelectParentMenu}
              options={
                selectParent.map(item => {
                  return {
                    value: item.id,
                    label: item.name
                  }
                })
              }
            />
          </Col>
        </Row>
        <br />
        <Row>
          <Col flex="100px"></Col>
          <Col flex="auto">
            <Button type="primary" htmlType="submit">
              Save <VerticalAlignBottomOutlined />
            </Button>
          </Col>
        </Row>
      </form>
    </div>
  );
};

export default FormMenu;

// {} []
