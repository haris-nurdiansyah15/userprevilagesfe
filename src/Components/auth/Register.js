import React, { useState, useContext, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { UserContext } from "../../Store/UserContext";
import iconLogin from "../../assets/img/login.png";
import { Form, Input, Button, Checkbox, Layout, Row, Col, Alert } from "antd";
import { UserOutlined, LockOutlined, MailOutlined } from "@ant-design/icons";
const { Content } = Layout;

const FormRegister = () => {
  const [, setUser] = useContext(UserContext);
  const [errorMessage, setErrorMessage] = useState("");
  const [fetchTrigger, setFetchTrigger] = useState(null);

  useEffect(() => {
    if (fetchTrigger) {
      setFetchTrigger(false);
    }
  });

  const onFinish = (values) => {
    axios
      .post("https://backendexample.sanbersy.com/api/register", {
        name: values.name,
        email: values.email,
        password: values.password,
      })
      .then((response) => {
        var user = response.data.user;
        var token = response.data.token;
        var currentUser = { name: user.username, email: user.email, token };
        setUser(currentUser);
        localStorage.setItem("user", JSON.stringify(currentUser));
      })
      .catch((err) => {
        const errorData = JSON.parse(err.response.data);
        setErrorMessage(errorData);
        setFetchTrigger(true);
      });
  };
  return (
    <>
      <Content
        style={{
          minHeight: 535,
          margin: "0 auto",
          width: "40%",
          marginBottom: 20,
        }}
      >
        <div
          className="site-layout-background"
          style={{
            padding: 30,
            marginTop: 60,
            marginBottom: 30,
            borderRadius: 50,
          }}
        >
          <Row justify="center">
            <Col span={6}>
              <img
                src={iconLogin}
                style={{ width: "100%", marginBottom: 30 }}
              />
            </Col>
          </Row>
          <Form name="normal_login" className="login-form" onFinish={onFinish}>
            <Form.Item
              name="name"
              validateStatus={errorMessage.name ? "error" : ""}
              help={errorMessage.name ? errorMessage.name[0] : null}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Name"
              />
            </Form.Item>
            <Form.Item
              name="email"
              validateStatus={errorMessage.email ? "error" : ""}
              help={errorMessage.email ? errorMessage.email[0] : null}
            >
              <Input
                prefix={<MailOutlined className="site-form-item-icon" />}
                placeholder="Email"
              />
            </Form.Item>
            <Form.Item
              name="password"
              validateStatus={errorMessage.password ? "error" : ""}
              help={errorMessage.password ? errorMessage.password[0] : null}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Password"
              />
            </Form.Item>

            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                style={{ width: "100%" }}
              >
                Register
              </Button>
              <br />
              <br />
              <Link to="/login" className="login-form-forgot">
                Return to login ?
              </Link>
            </Form.Item>
          </Form>
        </div>
      </Content>
    </>
  );
};

export default FormRegister;

// {} []
