import React, { useState, useContext, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { UserContext } from "../../Store/UserContext";
import iconLogin from "../../assets/img/login.png";
import { Form, Input, Button, Checkbox, Layout, Row, Col, Alert } from "antd";
import { MailOutlined, LockOutlined } from "@ant-design/icons";
const { Content } = Layout;

const FormLogin = () => {
  const [, setUser] = useContext(UserContext);
  const [errorMessage, setErrorMessage] = useState("");
  const [trigger, setTrigger] = useState(false);

  useEffect(() => {
    if (trigger) {
      setTrigger(false);
    }
  });

  const onFinish = (values) => {
    axios
      .post("http://localhost:8000/api/auth/login", {
        username: values.username,
        password: values.password,
      })
      .then((response) => {
        var user = response.data.data.user;
        var token = response.data.data.access_token;
        var access_menu = response.data.data.access_menu;
        var currentUser = { username: user.username, token, access_menu };
        setUser(currentUser);
        localStorage.setItem("user", JSON.stringify(currentUser));
      })
      .catch((err) => {
        const error = err.response.data.message;
        setErrorMessage(error);
        setTrigger(true);
      });
  };

  return (
    <>
      <Content style={{ minHeight: 535, margin: "0 auto", width: "40%"}}>
        <div
          className="site-layout-background"
          style={{
            padding: 30,
            marginTop: 60,
            borderRadius: 50,
          }}
        >
          <Row justify="center">
            <Col span={6}>
              <img
                src={iconLogin}
                style={{ width: "100%", marginBottom: 30 }}
              />
            </Col>
          </Row>
          <Form
            name="normal_login"
            className="login-form"
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
          >
            {errorMessage && (
              <Alert
                message={errorMessage}
                type="error"
                showIcon
                style={{ marginBottom: 20 }}
              />
            )}
            <Form.Item name="username">
              <Input
                prefix={<MailOutlined className="site-form-item-icon" />}
                placeholder="Username"
              />
            </Form.Item>
            <Form.Item name="password">
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Password"
              />
            </Form.Item>
            <Form.Item>
              <Form.Item name="remember" valuePropName="checked" noStyle>
                <Checkbox>Remember me</Checkbox>
              </Form.Item>
            </Form.Item>

            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                style={{ width: "100%" }}
              >
                Log in
              </Button>
              <br />
              <br />
              <Link to="/register" className="login-form-forgot">
                Dont't have an account ?
              </Link>
            </Form.Item>
          </Form>
        </div>
      </Content>
    </>
  );
};

export default FormLogin;

// {}
